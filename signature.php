<?php
/*
Plugin Name: Amen Theme stying
Description: Fixes avada theme for Amen. Use "amen-sidebar" 
Version: 0.0.1 
Plugin URI: 
Author URI: 
Author: 1844144@gmail.com
Text domain: 
*/
function theme_name1_scripts() {
    wp_enqueue_style( 'amen-style', plugin_dir_url(__FILE__).'css/amen.css' );
    wp_enqueue_script( 'amen-script',plugin_dir_url(__FILE__). 'js/amen.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'theme_name1_scripts' );

function add_footer() {
    wp_nav_menu (array( 
    	'menu'=> 4,
    	'link_before' => "<span>",
    	'link_after' => "</span>"
    	) );
}
add_action( 'wp_footer', 'add_footer', 0 );



