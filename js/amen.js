function do_clone (sel) {
	if ( jQuery(sel).length > 0){
		jQuery(sel + '> ul.sub-menu').clone().appendTo('.second-wrapper');

		// next if there is amen-sidebar or 'for-submenu'
		if (! do_add_submenu ('.second-wrapper > .sub-menu > li.current_page_ancestor > .sub-menu > li.current_page_ancestor > .sub-menu')) if (! do_add_submenu ('.second-wrapper > .sub-menu > li.current_page_ancestor > .sub-menu > li.current_page_item > .sub-menu')) if (! do_add_submenu('.second-wrapper > .sub-menu > li.current_page_ancestor > .sub-menu')) do_add_submenu( '.second-wrapper > .sub-menu > li.current_page_item > .sub-menu');
	}
	else return false;
}

function do_add_submenu (sel) {
	if ( jQuery(sel).length > 0){
		target = 'none';
		if ( jQuery('.amen-sidebar').length > 0 ) {
			target = '.amen-sidebar .fusion-fullwidth:first-of-type';
		}
		if ( jQuery('.for-submenu').length > 0){
			target = '.for-submenu';
		}
		if (target != 'none') {
			jQuery(sel).clone().addClass('third-level').appendTo(target);
			return true;
		}
		else {
			return false;
		}
	}
	else return false;
}

jQuery (function  () {
	jQuery('.fusion-header-v1').after("<div class='second-wrapper-container' > <div class='fusion-row second-wrapper'></div></div>");

	if (! do_clone('#menu-main-menu > .menu-item.current_page_ancestor')) do_clone( '#menu-main-menu > .menu-item.current_page_item');

	if ( jQuery('.amen-sidebar').length > 0 ) {
		jQuery('.amen-sidebar .fusion-fullwidth:first-of-type').clone().addClass('sidebar-duplicate').appendTo('.amen-sidebar .post-content');

	}

	jQuery('#menu-footer-menu').prependTo('#footer > .fusion-row');

	if (! do_clone('#menu-footer-menu > .menu-item.current_page_ancestor')) do_clone( '#menu-footer-menu > .menu-item.current_page_item');

	if ( jQuery('.amen-map').length > 0 ) {
		jQuery(jQuery('.amen-map.fusion-google-map')[0]).insertAfter('.second-wrapper-container');
		jQuery('body').addClass('amen-has-map');
	}

	// add additional class for sidebar images
	jQuery('.fusion-fullwidth-1 img, .fusion-fullwidth-2 img').wrap('<div class="sidebar-image-wrapper"></div>');

	

	
});